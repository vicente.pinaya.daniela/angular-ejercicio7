import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {


  boton:number=0
  constructor() { }


  ngOnInit(): void {
  }

  boton1():void {
    this.boton=1
  }

  boton2():void {
    this.boton=2
  }

  boton3():void {
    this.boton=3
  }
}
